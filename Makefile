BINARY_FILE?=crystal2influxdb
ENTRYPOINT?=cmd/crystal2influxdb/main.go
PROJECT_NAME?=crystal2influxdb
PROJECT_VERSION?=0.0.1


# Shortcuts / Aliases
RM=rm -r -v

.DEFAULT_GOAL := help

# echo function to logging Makefile events
define echo
	@tput setaf 11
	@echo "[${PROJECT_NAME}@v${PROJECT_VERSION}] - $(1)"
	@tput sgr0
endef

define echosuccess
	@tput setaf 40
	@echo "[${PROJECT_NAME}@v${PROJECT_VERSION}] - $(1)"
	@tput sgr0
endef


.PHONY: help
help: ## display current help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY: dev
dev:  ## execute code in dev mode
	@$(call echo, running ${PROJECT_NAME} in dev mode - Wait, please...)
	@go run $(ENTRYPOINT)
	@$(call echosuccess, ${PROJECT_NAME} started!)

.PHONY: build-arm
build-arm: ## build binary for arm architecture
	@$(call echo, build ${PROJECT_NAME} for ARMv5 architecture - Wait, please...)
	@env GOOS=linux GOARCH=arm GOARM=5 go build -o $(BINARY_FILE) $(ENTRYPOINT)
	@$(call echosuccess, ${PROJECT_NAME} build for ARMv5 architecture success!)

.PHONY: build
build: ## build binary
	@$(call echo, build ${PROJECT_NAME} - Wait, please...)
	@go build -o $(BINARY_FILE) $(ENTRYPOINT) 
	@$(call echosuccess, ${PROJECT_NAME} build success!)

.PHONY: deps 
deps: ## install dependencies
	@$(call echo, install dependencies for ${PROJECT_NAME} - Wait, please...)
	@go mod tidy
	@go mod vendor
	@$(call echosuccess, dependencies for ${PROJECT_NAME} installed)

.PHONY: clean
clean: ## remove build files
	@$(call echo, remove binary files - Wait, please...)
	$(RM) $(BINARY_FILE)
	$(RM) ./tmp
	@$(call echosuccess, remove binary files sucess!)
