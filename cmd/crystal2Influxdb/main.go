package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/jinzhu/configor"
	"gitlab.com/LouisBruge/crystal2Influxdb/models"
	"gitlab.com/LouisBruge/crystal2Influxdb/pkg/crystal"
	"gitlab.com/LouisBruge/crystal2Influxdb/pkg/influx"
)

func main() {
	scheduled := flag.Bool("scheduled", false, "activate / desactivate scheduled mod")
	configPath := flag.String("config", "config.yaml", "config path")

	flag.Parse()

	conf := models.Config{}

	if err := configor.Load(&conf, *configPath); err != nil {
		log.Fatal(err)
	}

	log.Printf("start application with the following configuration: %#v", conf)

	crystalClient := crystal.NewClient(
		crystal.OptionWithTimeout(time.Duration(conf.Crystal.Timeout)*time.Second),
		crystal.OptionWithHost(conf.Crystal.Host),
	)

	influxClient := influx.NewClient(
		influx.OptionWithAddress(conf.Influx.Address),
		influx.OptionWithDatabase(conf.Influx.Database),
		influx.OptionWithPrecision(conf.Influx.Precision),
		influx.OptionWithTimeout(time.Duration(conf.Influx.Timeout)*time.Second),
	)

	if !*scheduled {
		if e := export(crystalClient, influxClient); e != nil {
			log.Fatal(e)
		}
		os.Exit(0)
	}
	scheduledExport(crystalClient, influxClient, time.Duration(conf.Interval))
}

func scheduledExport(boxClient crystal.Client, influxClient influx.Client, interval time.Duration) {
	ticker := time.NewTicker(time.Duration(interval) * time.Minute)

	killChan := make(chan os.Signal, 2)
	signal.Notify(killChan, os.Interrupt, syscall.SIGTERM)

	for {
		select {
		case <-ticker.C:
			if e := export(boxClient, influxClient); e != nil {
				log.Printf("err: %v", e)
			}
		case <-killChan:
			log.Printf("kill by SIGTERM signal")
			os.Exit(0)
		}
	}
}

func export(boxClient crystal.Client, influxClient influx.Client) error {
	payload, err := boxClient.GetInformations()
	if err != nil {
		return err
	}

	return influxClient.Collect(*payload)
}
