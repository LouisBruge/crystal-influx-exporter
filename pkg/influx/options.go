package influx

import "time"

// OptionFunc type to defines function helpers to set options
type OptionFunc func(*influxClient)

// OptionWithDatabase option to specify the database
func OptionWithDatabase(database string) OptionFunc {
	return func(c *influxClient) {
		c.database = database
	}
}

// OptionWithPrecision option to specify the precision
func OptionWithPrecision(precision string) OptionFunc {
	return func(c *influxClient) {
		c.precision = precision
	}
}

// OptionWithAddress option to specify the influx address
func OptionWithAddress(address string) OptionFunc {
	return func(c *influxClient) {
		c.address = address
	}
}

// OptionWithTimeout option to specify connection timeout
func OptionWithTimeout(timeout time.Duration) OptionFunc {
	return func(c *influxClient) {
		c.timeout = timeout
	}
}
