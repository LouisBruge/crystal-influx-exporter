package influx

import (
	"time"

	influx "github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/LouisBruge/crystal2Influxdb/models"
)

// Client defines methods exposes by a influx client
type Client interface {
	Collect(models.Payload) error
}

type influxClient struct {
	address   string
	timeout   time.Duration
	database  string
	precision string
}

// NewClient returns a new instance of Client
func NewClient(opts ...OptionFunc) Client {
	c := &influxClient{}
	for _, opt := range opts {
		opt(c)
	}

	return c
}

// Collect points and them to the server
func (c *influxClient) Collect(payload models.Payload) error {
	bp, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database:  c.database,
		Precision: c.precision,
	})

	if err != nil {
		return err
	}

	{
		pts, err := c.collect("upload", payload.Upload, payload.Metadata)
		if err != nil {
			return nil
		}
		bp.AddPoints(pts)
	}

	{
		pts, err := c.collect("download", payload.Download, payload.Metadata)
		if err != nil {
			return nil
		}
		bp.AddPoints(pts)
	}
	return c.pushToServer(bp)
}

func (c *influxClient) collect(
	kind string,
	data models.Debit,
	meta models.Metadata,
) ([]*influx.Point, error) {
	tags := map[string]string{
		"flux":     kind,
		"ip":       meta.IP,
		"mac":      meta.Mac,
		"protocol": meta.Protocol,
		"mode":     meta.Mode,
	}

	pts := make([]*influx.Point, 0)

	{
		pt, err := influx.NewPoint("meta", map[string]string{"mac": meta.Mac}, map[string]interface{}{
			"ip":       meta.IP,
			"protocol": meta.Protocol,
			"mode":     meta.Mode,
		})
		if err != nil {
			return nil, err
		}
		pts = append(pts, pt)
	}

	{
		pt, err := influx.NewPoint("atm", tags, map[string]interface{}{"value": data.ATM})
		if err != nil {
			return nil, err
		}
		pts = append(pts, pt)
	}

	{
		pt, err := influx.NewPoint("noise", tags, map[string]interface{}{"value": data.NoiseMargin})
		if err != nil {
			return nil, err
		}
		pts = append(pts, pt)
	}

	{
		pt, err := influx.NewPoint("mitigation", tags, map[string]interface{}{"value": data.Mitigation})
		if err != nil {
			return nil, err
		}
		pts = append(pts, pt)
	}

	{
		pt, err := influx.NewPoint("HEC", tags, map[string]interface{}{"value": data.HEC})
		if err != nil {
			return nil, err
		}
		pts = append(pts, pt)
	}

	{
		pt, err := influx.NewPoint("CRC", tags, map[string]interface{}{"value": data.CRC})
		if err != nil {
			return nil, err
		}
		pts = append(pts, pt)
	}

	{
		pt, err := influx.NewPoint("FEC", tags, map[string]interface{}{"value": data.FEC})
		if err != nil {
			return nil, err
		}
		pts = append(pts, pt)
	}
	return pts, nil
}

func (c *influxClient) pushToServer(bp influx.BatchPoints) error {
	client, err := influx.NewHTTPClient(influx.HTTPConfig{
		Addr:    c.address,
		Timeout: c.timeout,
	})
	if err != nil {
		return err
	}

	defer client.Close()

	if e := client.Write(bp); e != nil {
		return e
	}
	return nil
}
