package crystal

import "time"

// OptionFunc helper type for option function
type OptionFunc func(c *httpClient)

// OptionWithTimeout option function to set a timeout
func OptionWithTimeout(timeout time.Duration) OptionFunc {
	return func(c *httpClient) {
		c.timeout = timeout
	}
}

// OptionWithHost option function to set the crystal host
func OptionWithHost(host string) OptionFunc {
	return func(c *httpClient) {
		c.host = host
	}
}
