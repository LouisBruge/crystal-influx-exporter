package crystal

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"time"

	"gitlab.com/LouisBruge/crystal2Influxdb/models"
	"golang.org/x/text/encoding/charmap"
)

// Client defines methods exposes by a crystal client
type Client interface {
	GetInformations() (*models.Payload, error)
}

// httpClient is a concrete implementaiton of Client interface using underlying http client
type httpClient struct {
	timeout time.Duration
	host    string
}

// EndPoint crystal information endpoint
const EndPoint string = "pub/fbx_info.txt"

// NewClient generate a new client for crystal
func NewClient(opts ...OptionFunc) Client {
	c := &httpClient{}

	for _, opt := range opts {
		opt(c)
	}

	return c
}

func (c *httpClient) GetInformations() (*models.Payload, error) {
	client := http.Client{
		Timeout: c.timeout,
	}

	resp, err := client.Get(c.endpoint())
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(charmap.ISO8859_1.NewDecoder().Reader(resp.Body))
	if err != nil {
		log.Fatal(err)
	}

	return c.parse(string(body))
}

func (c httpClient) endpoint() string {
	return fmt.Sprintf("http://%s/%s", c.host, EndPoint)
}

func (c httpClient) parse(body string) (*models.Payload, error) {
	var payload models.Payload

	/*
	 * Metadata
	 */
	var protocolRegex = regexp.MustCompile(`Protocole\s+([\w+]+)`)
	protocol := protocolRegex.FindStringSubmatch(body)
	if len(protocol) < 2 {
		return nil, errors.New("failed to extract protocol")
	}
	payload.Metadata.Protocol = protocol[1]

	var modeRegex = regexp.MustCompile(`Mode\s+(\w+)\s+\n`)
	mode := modeRegex.FindStringSubmatch(body)
	if len(mode) < 2 {
		return nil, errors.New("failed to extract mode")
	}
	payload.Metadata.Mode = mode[1]

	var ipRegex = regexp.MustCompile(`Adresse\sIP\s+([0-9.]+)`)
	ip := ipRegex.FindStringSubmatch(body)
	if len(ip) < 2 {
		return nil, errors.New("failed to extract ip")
	}
	payload.Metadata.IP = ip[1]

	var macRegex = regexp.MustCompile(`Adresse\sMAC\sFreebox\s+([0-9A-E:]+)`)
	mac := macRegex.FindStringSubmatch(body)
	if len(mac) < 2 {
		return nil, errors.New("failed to extract mac address")
	}
	payload.Metadata.Mac = mac[1]

	/*
	 * Load
	 */
	var atmRegex = regexp.MustCompile(`ATM\s+([0-9]+)\skb\/s\s+([0-9]+)\skb\/s`)
	upAtm, downAtm, err := c.parseFloat(atmRegex, body)
	if err != nil {
		return nil, errors.New("failed to extract ATM")
	}
	payload.Upload.ATM = upAtm
	payload.Download.ATM = downAtm

	var noiseMargin = regexp.MustCompile(`Marge\sde\sbruit\s+([0-9.]+)\sdB\s+([0-9.]+)\sdB`)
	upNoise, downNoise, err := c.parseFloat(noiseMargin, body)
	if err != nil {
		return nil, errors.New("failed to extract Noise Margin")
	}
	payload.Upload.NoiseMargin = upNoise
	payload.Download.NoiseMargin = downNoise

	var mitigationRegex = regexp.MustCompile(`Atténuation\s+([0-9.]+)\sdB\s+([0-9.]+)\sdB`)
	mitigationUp, mitigationDown, err := c.parseFloat(mitigationRegex, body)
	if err != nil {
		return nil, errors.New("failed to extract Mitigation")
	}
	payload.Upload.Mitigation = mitigationUp
	payload.Download.Mitigation = mitigationDown

	var fecRegex = regexp.MustCompile(`FEC\s+([0-9]+)\s+([0-9]+)`)
	fecUp, fecDown, err := c.parseInt(fecRegex, body)
	if err != nil {
		return nil, err
	}
	payload.Upload.FEC = fecUp
	payload.Download.FEC = fecDown

	var crcRegex = regexp.MustCompile(`CRC\s+([0-9]+)\s+([0-9]+)`)
	crcUp, crcDown, err := c.parseInt(crcRegex, body)
	if err != nil {
		return nil, err
	}
	payload.Upload.CRC = crcUp
	payload.Download.CRC = crcDown

	var hecRegex = regexp.MustCompile(`HEC\s+([0-9]+)\s+([0-9]+)`)
	hecUp, hecDown, err := c.parseInt(hecRegex, body)
	if err != nil {
		return nil, err
	}
	payload.Upload.HEC = hecUp
	payload.Download.HEC = hecDown

	return &payload, nil
}

func (c httpClient) parseFloat(regex *regexp.Regexp, body string) (float64, float64, error) {
	res := regex.FindStringSubmatch(body)
	if len(res) < 3 {
		return 0, 0, errors.New("failed to extract data")
	}

	up, err := strconv.ParseFloat(res[1], 64)
	if err != nil {
		return 0, 0, err
	}

	down, err := strconv.ParseFloat(res[2], 64)
	if err != nil {
		return 0, 0, err
	}

	return up, down, nil
}

func (c httpClient) parseInt(regex *regexp.Regexp, body string) (int, int, error) {
	res := regex.FindStringSubmatch(body)
	if len(res) < 3 {
		return 0, 0, errors.New("failed to extract data")
	}

	up, err := strconv.Atoi(res[1])
	if err != nil {
		return 0, 0, err
	}

	down, err := strconv.Atoi(res[2])
	if err != nil {
		return 0, 0, err
	}

	return up, down, nil
}
