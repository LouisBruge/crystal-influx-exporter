package models

// Config defines configuration for crystal2Influxdb instance
type Config struct {
	Crystal  CrystalConfig `json:"crystal" yaml:"crystal"`
	Influx   InfluxConfig  `json:"influx" yaml:"influx"`
	Interval int           `json:"refreshIntervalMin" yaml:"refreshIntervalMin" env:"REFRESH_INTERVAL_MIN" default:"60"`
}

// CrystalConfig defines configuration for the crystal client
type CrystalConfig struct {
	Timeout int    `json:"timeoutSec" yaml:"timeoutSec" env:"CRYSTAL_TIMEOUT_SEC" default:"30"`
	Host    string `json:"host" yaml:"host" env:"CRYSTAL_HOST" default:"mafreebox.freebox.fr"`
}

// InfluxConfig defines configuration of the influx client
type InfluxConfig struct {
	Timeout   int    `json:"timeoutSec" yaml:"timeoutSec" env:"INFLUX_TIMEOUT_SEC" default:"30"`
	Database  string `json:"database" yaml:"database" env:"INFLUX_DATABASE" required:"true"`
	Address   string `json:"address" yaml:"address" env:"INFLUX_ADDRESS" default:"http://localhost:8086"`
	Precision string `json:"precision" yaml:"precision" env:"INFLUX_PRECISION" default:"h"`
}
