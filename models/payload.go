package models

// Payload represent the payload extracted from Crystal box
type Payload struct {
	Metadata `json:"metadata"`
	Upload   Debit `json:"upload"`
	Download Debit `json:"download"`
}

// Metadata represent metadata associated to the Crystal box
type Metadata struct {
	Protocol string `json:"protocol"`
	Mode     string `json:"mode"`
	IP       string `json:"ip"`
	Mac      string `json:"mac"`
}

// Debit represent informations associated to a debit
type Debit struct {
	ATM         float64 `json:"atm"`
	NoiseMargin float64 `json:"noiseMargin"`
	Mitigation  float64 `json:"mitigation"`
	FEC         int     `json:"FEC"`
	CRC         int     `json:"CRC"`
	HEC         int     `json:"HEC"`
}
