module gitlab.com/LouisBruge/crystal2Influxdb

go 1.15

require (
	github.com/influxdata/influxdb1-client v0.0.0-20200827194710-b269163b24ab
	github.com/jinzhu/configor v1.2.0
	golang.org/x/text v0.3.3
)
